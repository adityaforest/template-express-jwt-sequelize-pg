Step to continue :

1. generate model sesuai kebutuhan
yarn sequelize model:generate --name NamaTable --attributes namakolom1:tipedatacontohstring,namakolom2:tipedata,dst (bikin model/table + kolom)

2. create database in local environment (jangan lupa edit config file)
yarn sequelize db:create (bikin db nya sesuai config.json)

3. generate seed sesuai kebutuhan dan edit seedernya
yarn sequelize-cli seed:generate --name namaseeder (bikin file seeder)

4. create .env file contoh isinya
DB_USERNAME = asquzngy123
DB_PASSWORD = CeZ4CcrFdkzs_N1UTnEOSb_3vDXiC5V912
DB_DATABASE = asquzngy1
DB_HOST = satao.db.elephantsql.com
DB_DIALECT = postgres
PORT = 3001
JWT_SECRET_KEY = huahahaha

dev:
sequelize db:migrate
sequelize db:seed:all
