const express = require('express')
const router = express.Router()

const userController = require('./../controllers/user.controller')

// Authentication
router.get('/users/:id', userController.show)
router.delete('/users/:id', userController.delete)

module.exports = router