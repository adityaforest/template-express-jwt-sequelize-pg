const { user, point } = require('./../../models')
const { responseSuccess, responseError } = require('../utils/response.formatter.util')

module.exports = {
    show: async (req, res) => {
        try {
            const users = await user.findOne({
                where: { id: req.params.id },
                attributes: { exclude: ['password'] },
                include: [
                    {
                        model: point,
                        attributes: ['game_id', 'score'],
                    }
                ],
            })

            if (!users) {
                res.status(404).json(responseError('User not found'))
            } else {
                res.json(responseSuccess(users))
            }
        } catch (err) {
            console.log(err.message)
            res.status(404).json(responseError('User not found'))
        }
    },
    delete: async (req, res) => {
        try {
            const id = req.params.id
            const users = await user.findOne({ where: { id } })
            if (!users) {
                res.status(404).json(responseError('User not found'))
            } else {
                await user.destroy({ where: { id } })
                res.json(responseSuccess({ id }))
            }
        } catch (err) {
            res.json(responseError('Delete user failed'))
        }
    }
}