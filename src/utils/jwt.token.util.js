const jwt = require('jsonwebtoken')

const { user } = require('../../models')

module.exports = {
    generateToken: async (id) => {
        const users = await user.findByPk(id)

        const token = jwt.sign(
            {
                id: users.id,
                uuid: users.uuid,
                firstName: users.firstName,
                lastName: users.lastNamee
            },
            process.env.JWT_SECRET_KEY,
            {
                expiresIn: '24h'
            }
        )

        return token;
    },
    getUserVerified: (token) => {
        if (token) {
            try {
                verify = jwt.verify(token, process.env.JWT_SECRET_KEY)
                return verify
            } catch (err) {
                return false
            }
        }

        return false

    },
    getTokenFromHeader: (token) => {
        return token.split(' ')[1]
    }
}