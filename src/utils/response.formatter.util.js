const responseDefaults = () => {
    const response = {
        'status': 'SUCCESS'
    }

    return response
}

module.exports = {
    responseSuccess: (data = null) => {
        const response = responseDefaults()

        response.data = data

        return response
    },
    responseError: (message = null) => {
        const response = responseDefaults()

        response.status = 'FAILED'
        response.message = message

        return response
    }
}